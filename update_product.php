<?php 
include 'db_connect.php';

$product_id = $_GET['product_id'];
$prd = query("select * from products where product_id='$product_id'")[0];



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Product</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
  <a href="index.php">Dashboard</a>
  <a href="product.php">Table Product</a>

  <h2>Update Product</h2>

    <form action="" id="updateProduct" method="post">        
        <label for="product_name">Product Name:</label>
        <input type="text" id="product_name" name="product_name" value="<?php echo $prd['product_name']; ?>" required>
        <br>

        <label for="price">Price:</label>
        <input type="number" id="price" name="price" value="<?php echo $prd['price']; ?>" required>
        <br>

        <label for="stock">Stock:</label>
        <input type="number" id="stock" name="stock" value="<?php echo $prd['stock']; ?>" required>
        <br>

        <label for="unit">Unit:</label>
        <input type="text" id="unit" name="unit" value="<?php echo $prd['unit']; ?>" required>
        <br>

        <button type="submit" value="update" name="update" onclick="updateProduct()">Update Product</button>
    </form>
    
    <?php
      if(isset($_POST['update'])){        
        $product_name = $_POST['product_name'];
        $price = $_POST['price'];
        $stock = $_POST['stock'];
        $unit = $_POST['unit'];
        
        mysqli_query($conn, "update products set product_name='$product_name', price='$price', stock='$stock', unit='$unit' where product_id='$product_id'");
        echo "<script>alert('data successfully updated');window.location='product.php';</script>";
      }
      ?>

<script>
        function updateProduct() {            
            $.ajax({
                type: "POST",
                url: "product.php",
                data: $("#updateProduct").serialize(),
                success: function(response) {
                    if (response === "success") {
                        alert("Update Successful.");
                    } else {
                        // alert("Failed to update store.");
                    }
                },
                error: function(error) {
                    console.log(error);
                    alert("Failed update data customer");
                }
            });
        }
    </script>


      </body>
    </html>