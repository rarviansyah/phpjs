<?php
// session_start();
include 'db_connect.php';

$query = "SELECT * FROM cart";
$result = $conn->query($query);

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['cart'])) {
    // Decode the JSON data received from the client
    $cart = json_decode($_POST['cart'], true);

    // Update the session cart data
    // $_SESSION['cart'] = $cart;
    $product_id = $_POST['product_id'];
    $product_name = $_POST['product_name'];
    $price = $_POST['price'];
    $unit = 1;
    
    $sql = "INSERT INTO cart (product_id, product_name, price, unit)
    SELECT p.product_id, p.product_name, p.price, 1 as unit
    FROM product p
    LEFT JOIN cart c ON p.product_id = c.product_id
    WHERE p.product_id = '$product_id' AND c.product_id IS NULL";

    if ($mysqli->query($sql) === TRUE) {
        echo "Produk berhasil ditambahkan ke keranjang";
    } else {
        echo "Error: " . $sql . "<br>" . $mysqli->error;
    }

    // Respond with a success message or any necessary data
    echo "Cart updated successfully!";
} else {
    // Respond with an error message if the request is not a POST request or if 'cart' is not set
    // header('HTTP/1.1 400 Bad Request');
    // echo "Invalid request.";
}
// if ($_SERVER["REQUEST_METHOD"] == "POST") {
//     // Tangkap data dari POST request
//     $product_id = $_POST['product_id'];
//     $product_name = $_POST['product_name'];
//     $price = $_POST['price'];
//     $unit = 1; // Misalnya, setiap kali menambahkan ke cart, quantity diatur menjadi 1

//     // Query untuk memasukkan data ke dalam tabel cart
//     $sql = "INSERT INTO cart (product_id, product_name, price, unit)
//     SELECT p.product_id, p.product_name, p.price, 1 as unit
//     FROM product p
//     LEFT JOIN cart c ON p.product_id = c.product_id
//     WHERE p.product_id = '$product_id' AND c.product_id IS NULL";

//     if ($mysqli->query($sql) === TRUE) {
//         echo "Produk berhasil ditambahkan ke keranjang";
//     } else {
//         echo "Error: " . $sql . "<br>" . $mysqli->error;
//     }
// }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Your existing head section... -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>

<h2>Cart</h2>

<a href="index.php">Dashboard</a>
<a href="product.php">Table Product</a>

<form action="" method="post">
    <?php
    if (isset($_SESSION['cart']) && is_array($_SESSION['cart'])) {
        echo '<table border="5">
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Unit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>';

         foreach ($_SESSION['cart'] as $key => $item) {
        echo "<tr>";
        echo "<td>" . $item['productId'] . "</td>";
        echo "<td>" . $item['productName'] . "</td>";
        echo "<td>" . $item['price'] . "</td>";
        echo "<td>";
        echo "<button class='unit-btn' data-action='minus' data-key='$key' >-</button>";
        echo "<span class='unit' data-key='$key'>" . $item['unit'] . "</span>";
        echo "<button class='unit-btn' data-action='plus' data-key='$key' >+</button>";
        echo "</td>";
        echo "<td><input type='checkbox' class='include-checkbox' name='include[]' value='" . $item['productId'] . "' checked></td>";
        echo "</tr>";
    } 

        echo '</tbody>
              </table>
              <p>Total Price: Rp <span id="totalPrice"></span></p>
              <button type="button" id="buyButton">Buy</button>';
    } else {
        echo "<p>Your cart is empty.</p>";
    }
    ?>
</form>

<script>
   $(document).ready(function () {
    $('.unit-btn').on('click', function () {
        event.preventDefault();
        var action = $(this).data('action');
        var key = $(this).data('key');
        var unitElement = $('.unit[data-key="' + key + '"]');
        var currentUnit = parseInt(unitElement.text());

        $.ajax({
            url: 'update_unit.php', // Ganti dengan nama file yang menangani pembaruan unit di sisi server
            type: 'POST',
            data: {
                action: action,
                key: key
            },
            success: function (response) {
                console.log(response);
                if (response === 'success') {
                    if (action === 'plus') {
                        unitElement.text(currentUnit + 1);
                    } else if (action === 'minus' && currentUnit > 1) {
                        unitElement.text(currentUnit - 1);
                    }
                    updateTotalPrice();
                } else {
                    alert('Gagal memperbarui unit.');
                }
            }
        });
    });

    function updateTotalPrice() {
        var rows = $('tbody tr');
        var totalPrice = 0;

        rows.each(function () {
            var price = parseFloat($(this).find('td:eq(2)').text());
            var unit = parseInt($(this).find('.unit').text());
            totalPrice += price * unit;
        });

        $('#totalPrice').text(totalPrice.toFixed(2));
    }
});

document.addEventListener('DOMContentLoaded', function() {
    const checkboxes = document.querySelectorAll('.include-checkbox');
    const totalPriceSpan = document.getElementById('totalPrice');

    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            updateTotalPrice();
        });
    });

    // Initial update
    // updateTotalPrice();

    function updateTotalPrice() {
        let total = 0;
        checkboxes.forEach(function(checkbox) {
            if (checkbox.checked) {
                const priceCell = checkbox.closest('tr').querySelector('td:nth-child(3)');
                if (priceCell) {
                    const price = parseFloat(priceCell.innerText);
                    total += price;
                }
            }
        });
        totalPriceSpan.innerText = total.toFixed(2);
    }
});

    // Initial update
    // updateTotalPrice(); 

    // Add click event to the Buy button
    document.getElementById('buyButton').addEventListener('click', function() {
        buyProduct();
    });

function buyProduct() {
    const checkboxes = document.querySelectorAll('.include-checkbox:checked');
    checkboxes.forEach(function(checkbox) {
        const productId = checkbox.value;
        if (confirm("Are you sure you want to buy this product?")) {
            $.ajax({
                type: "POST",
                url: "buy_product.php",
                data: { product_id: productId },
                dataType: 'json', // Expect JSON response
                success: function(response) {
                    console.log(response);
                    if (response.status === "success") {
                        // Remove the row for the bought product
                        const row = checkbox.closest('tr');
                        if (row) {
                            row.remove();
                        }
                        $('#totalPrice').hide();
                        // alert("buying successfully")
                        // Update total price after successful purchase

                        // Display success message on buy_product.php
                        const successMessage = document.createElement('p');
                        successMessage.textContent = response.message;
                        document.body.appendChild(successMessage);
                    } else {
                        alert("Failed to buy product.");
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    alert("Failed to buy product. Server error.");
                }
            });
        }
    });
}
</script>

</body>
</html>