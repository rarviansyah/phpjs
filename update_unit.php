<?php
// File: update_unit.php
// $query = "SELECT * FROM products";
// $result = $conn->query($query);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = $_POST['action'];
    $key = $_POST['key'];

    // Lakukan validasi dan pembaruan unit di sini sesuai dengan logika aplikasi Anda
    // Contoh: simpan nilai baru ke database atau sesuai kebutuhan aplikasi Anda

    // Beri respons success jika pembaruan berhasil
    echo 'success';
} else {
    // Beri respons failure jika metode yang digunakan bukan POST
    echo 'failure';
}
