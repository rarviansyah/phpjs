<?php
include 'db_connect.php';

// Query untuk mendapatkan daftar produk
$query = "SELECT * FROM products";
$result = $conn->query($query);

if (isset($_POST['product_id'])) {
    $product_id = $_POST['product_id']; 
    $delete_query = "DELETE FROM products WHERE product_id = '$product_id'";
    if ($conn->query($delete_query) === TRUE) {
        echo "success";
    } else {
        echo "error";
    }
    exit; 
}

// $product_id = $_POST['product_id'];
// $product_name = $_POST['product_name'];
// $price = $_POST['price'];
// $stock = $_POST['stock'];
// $unit = $_POST['unit'];

// // Prepare the SQL statement to insert the data into the `cart` table
// $stmt = $conn->prepare("INSERT INTO cart (product_id, product_name, price, stock, unit) VALUES (?, ?, ?, ?, ?)");
// $stmt->bind_param("issss", $product_id, $product_name, $price, $stock, $unit);

// // Execute the SQL statement and check for errors
// if ($stmt->execute()) {
//     echo "Product added to cart successfully.";
// } else {
//     echo "Error: " . $stmt->error;
// }

// $conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
    <a href="add_product.php">Add Product</a>
    <a href="index.php">Dashboard</a>
    <a href="cart.php">Cart</a>
    <!-- Container untuk menampilkan produk -->
    <div id="product-container">
        <h2>Product List</h2>
        <?php if ($result->num_rows > 0) : ?>
            <table border="1">
                <thead>
                    <tr>
                        <th>Product ID</th>                        
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Unit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while ($row = $result->fetch_assoc()) : ?>
                        <tr id="row_<?php echo $row['product_id']; ?>">
                            <td><?php echo $row['product_id']; ?></td>                            
                            <td><?php echo $row['product_name']; ?></td>
                            <td><?php echo $row['price']; ?></td>
                            <td><?php echo $row['stock']; ?></td>
                            <td><?php echo $row['unit']; ?></td>
                            <td>
                                <button onclick="deleteProduct('<?php echo $row['product_id']; ?>')"> Delete </button>
                                <button><a href="update_product.php?product_id=<?php echo $row['product_id']; ?>">Update</a></button>                
                                <button onclick="addToCart(
                                    '<?php echo $row['product_id']; ?>',
                                    '<?php echo $row['product_name']; ?>',
                                    '<?php echo $row['price']; ?>',
                                    '<?php echo $row['stock']; ?>',
                                    '<?php echo $row['unit']; ?>'
                                )">Add To Cart</button>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        <?php else : ?>
            <p>No products available</p>
        <?php endif; ?>
    </div>

    <script>
        function deleteProduct(productId) {
            if (confirm("Are you sure you want to delete this product?")) {
                $.ajax({
                    type: "POST",
                    url: "product.php",
                    data: { product_id: productId },
                    success: function(response) {
                        console.log(response);
                        if (response === "success") {
                            $("#row_" + productId).remove();
                            alert("Delete Successfully");
                        } else {
                            alert("Failed to delete product.");
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                        alert("Failed to delete product. Server error.");
                    }
                });
            }
        }

        function addToCart(productId, productName, price, stock, unit) {
            let userId = 1;  // Change this to actual user ID retrieval logic

            $.ajax({
                type: "POST",
                url: "cart.php",
                data: {
                    userId: userId,
                    productId: productId,
                    productName: productName,
                    price: price,
                    stock: stock,
                    unit: unit
                },
                success: function(response) {
                    alert("Product added to cart successfully!");
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    alert("Failed to add product to cart.");
                }
            });
        }
    </script>
</body>
</html>
