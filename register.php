<?php
include 'db_connect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $phone_number = $_POST['phone_number'];

    // Auto-generate account code
    $query = "SELECT MAX(SUBSTRING(account_code, 6)) as max_code FROM users";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $max_code = $row['max_code'];
    $account_code = "AC" . str_pad(($max_code + 1), 4, '0', STR_PAD_LEFT);

    $insert_query = "INSERT INTO users (first_name, last_name, username, email, phone_number, password, account_code) VALUES ('$first_name', '$last_name', '$username', '$email', '$phone_number', '$password', '$account_code')";
    if ($conn->query($insert_query) === TRUE) {
        echo "Registration successful. Your account code is: $account_code";
    } else {
        echo "Error: " . $insert_query . "<br>" . $conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Registrations</title>
</head>
<body>
  <div class="container">
    <h2>Register</h2>
        <form action="" method="post" autocomplete="off" id="register_form">
      <label for="first_name">First Name :</label><br>
      <input type="text" name="first_name" id="first_name" required><br>
      <label for="last_name">Last Name :</label><br>
      <input type="text" name="last_name" id="last_name" required><br>
      <label for="username">Username :</label><br>
      <input type="text" name="username" id="username" required><br>
      <label for="email">Email :</label><br>
      <input type="text" name="email" id="email" required><br>
      <label for="password">Password:</label><br>
      <input type="password" name="password" id="password" required><br>
      <label for="phone_number">Phone Number :</label><br>
      <input type="text" name="phone_number" id="phone_number" required><br>
      <button type="submit" onclick="register()">Register</button>
    </form>
    <br>
    <a href="login.php">Login</a>
  </div>

  <script src="script.js"></script>
</body>
</html>