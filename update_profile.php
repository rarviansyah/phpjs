<?php
include 'db_connect.php';

$user_id = $_SESSION['user_id'];

$prd = query("select * from users where id='$user_id'")[0];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Profile</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
    <a href="index.php">Dashboard</a>
    <a href="profile.php">Profile</a>

    <h2>Update Profile</h2>
    <form action="" method="post" enctype="multipart/form-data">
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" id="first_name" value="<?= $prd['first_name'] ?>" required>
        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" id="last_name" value="<?= $prd['last_name']?>" required><br><br>
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" value="<?= $prd['username'] ?>">
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" value="<?= $prd['email'] ?>"><br><br>
        <label for="address">Address:</label>
        <input type="text" name="address" id="address" value="<?= $prd['address'] ?>" required>
        <label for="post_code">Post Code:</label>
        <input type="text" name="post_code" id="post_code" value="<?= $prd['post_code']?>" required><br><br>
        <!-- <button id="get-location">Get Location</button>

        <p id="latitude"></p>
        <p id="longitude"></p> -->
        <input type="submit" name="update">
    </form>

    <?php 
      if(isset($_POST['update'])){
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $post_code = $_POST['post_code'];
        // $latitude = $_POST['latitude'];
        // $longitude = $_POST['longitude'];

        mysqli_query($conn, "update users set first_name='$first_name', last_name='$last_name', username='$username', email='$email', address='$address', post_code='$post_code' where id='$user_id'");

        echo "<script>alert('data successfully updated');window.location='profile.php';</script>";
      }
      ?>  

    <script>
  //      $('#get-location').click(() => {
  //       if (!navigator.geolocation) return alert('Geolocation is not supported.');

  //       navigator.geolocation.getCurrentPosition(
  // (position) => {
  //   $('#latitude').html(`Latitude: ${position.coords.latitude}`);
  //   $('#longitude').html(`Longitude: ${position.coords.longitude}`);
  // },
  // (error) => {
  //   console.error('Error getting location:', error);
  // })
  //      });

  </script>
</body>
</html>
