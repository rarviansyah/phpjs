<?php
include 'db_connect.php';

$user_id = $_SESSION['user_id'];

$query = "SELECT first_name, last_name, username, email, address, post_code, latitude, longitude FROM users where id='$user_id'";
$result = $conn->query($query);

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $username = $row["username"];
        $email = $row["email"];
        $address = $row["address"];
        $post_code = $row["post_code"];
        $latitude = $row['latitude'];
        $longitude = $row['longitude'];
    }
} else {
    echo "Tidak ada data pengguna dalam tabel.";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Profile</title>
  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
  <a href="index.php">Dashboard</a>
  
  <h2>Profile</h2>
  <a href="update_profile.php?=<?= $user_id ?>">Update Profile</a><br><br>
    <form action="" method="post" enctype="multipart/form-data">
    <label for="first_name">First Name :</label>
      <input type="text" name="first_name" id="first_name" value="<?= $first_name?>" required>
      <label for="last_name">Last Name :</label>
      <input type="text" name="last_name" id="last_name" value="<?= $last_name ?>" required><br><br>
      <label for="username">Username :</label>
      <input type="text" name="username" id="username" value="<?= $username ?>">
      <label for="email">Email :</label>
      <input type="text" name="email" id="email" value="<?= $email ?>"><br><br>
      <label for="address">Address:</label>
      <input type="text" name="address" id="address" value="<?= $address ?>" required>
      <label for="post_code">Post Code:</label>
      <input type="text" name="post_code" id="post_code" value="<?= $post_code ?>" required><br><br>
      
    </form>    
    <button id="get-location">Get Location</button>

      <p id="latitude"></p>
      <p id="longitude"></p>

    <script>
       $('#get-location').click(() => {  
          if (!navigator.geolocation)
            return alert("Geolocation is not supported.");

          navigator.geolocation.getCurrentPosition((position) => {
            $("#latitude").html(`Latitude: ${position.coords.latitude}`);
            $("#longitude").html(`Longitude: ${position.coords.longitude}`);
          });
        });

  </script>
</body>
</html>