<?php
include 'db_connect.php';

// if ($_SERVER['REQUEST_METHOD'] === 'POST') {
//     // Ambil data dari formulir
//     $product_id = $_POST['product_id'];
//     $store_id = $_POST['store_id'];
//     $product_name = $_POST['product_name'];
//     $price = $_POST['price'];
//     $stock = $_POST['stock'];
//     $unit = $_POST['unit'];

//     // Lakukan validasi atau pembersihan data sesuai kebutuhan

//     // Tambahkan produk ke database
//     $insertQuery = "INSERT INTO products (product_id, store_id, product_name, price, stock, unit) VALUES ('$product_id', '$store_id', '$product_name', $price, $stock, '$unit')";
    
//     if ($conn->query($insertQuery)) {
//         // Dapatkan ID produk yang baru ditambahkan
//         $newProductId = $conn->insert_id;

//         // Ambil data produk yang baru ditambahkan
//         $selectQuery = "SELECT * FROM products WHERE product_id = $newProductId";
//         $result = $conn->query($selectQuery);

//         if ($result->num_rows > 0) {
//             $row = $result->fetch_assoc();
//             echo json_encode($row); // Mengembalikan data produk dalam format JSON
//         } else {
//             echo 'Error retrieving product data';
//         }
//     } else {
//         echo 'Error adding product: ' . $conn->error;
//     }
// } else {
//     // echo 'Invalid request';
// }

if($_SERVER["REQUEST_METHOD"] == "POST"){    
    $store_id = $_POST['store_id'];
    $product_name = $_POST['product_name'];
    $price = $_POST['price'];
    $stock = $_POST['stock'];
    $unit = $_POST['unit'];

    // SQL Query untuk menambahkan produk ke dalam tabel produk
    $sql = "INSERT INTO products (product_name, price, stock, unit)
            VALUES ('$product_name', '$price', '$stock', '$unit')";

    // Jalankan query
    if ($conn->query($sql) === TRUE) {
        header("Location: product.php?message=Product added successfully");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product Form</title>
</head>
<body>

<a href="index.php">Dashboard</a>
<a href="product.php">Table Product</a>

<h2>Add Product</h2>

<form action="add_product.php" method="post">
    <label for="store_id">Store ID:</label>
    <input type="text" id="store_id" name="store_id" required><br>

    <label for="product_name">Product Name:</label>
    <input type="text" id="product_name" name="product_name" required><br>

    <label for="price">Price:</label>
    <input type="number" step="0.01" id="price" name="price" required><br>

    <label for="stock">Stock:</label>
    <input type="number" id="stock" name="stock" required><br>

    <label for="unit">Unit:</label>
    <input type="text" id="unit" name="unit" required><br>

    <input type="submit" name="add_product" value="Add Product">
</form>

</body>
</html>
