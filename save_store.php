<?php 
include 'db_connect.php';

$sql = "SELECT * FROM stores";
$result = $conn->query($sql);

if (isset($_POST['store_id'])) {
    $store_id = $_POST['store_id'];
    $sql = "DELETE FROM stores WHERE store_id = $store_id";
    if ($conn->query($sql) === TRUE) {
        echo "success";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
} else {
    // echo "missing_store_id";
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Store Table</title>
  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
  <a href="store.php">Add Store</a>
  <a href="index.php">Dashboard</a>

  
  <h2>Store Table</h2>

  <?php
  // Tampilkan pesan setelah submit form
  if (isset($_GET['message'])) {
      echo '<p>' . $_GET['message'] . '</p>';
  }
  ?>

<table border="1">
    <tr>
        <th>Store ID</th>
        <th>Store Name</th>
        <th>Owner Name</th>
        <th>Store Address</th>
    </tr>

    <?php
    // Tampilkan data dari hasil query
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<tr>
                    <td>{$row['store_id']}</td>
                    <td>{$row['store_name']}</td>
                    <td>{$row['owner_name']}</td>
                    <td>{$row['store_address']}</td>
                    <td><button onclick='deleteStore(" . $row['store_id'] . ")'> Delete </button></td>
                    <td><button> <a href='update_store.php?store_id=" . $row['store_id'] . "'> Update </button></td>
                </tr>";
        }
    } else {
        echo "<tr><td colspan='4'>No stores found</td></tr>";
    }
    ?>
</table>

<script>
        function deleteStore(storeId) {
        if (confirm("Are you sure you want to delete this store?")) {
            $.ajax({
                type: "POST",
                url: "save_store.php",
                data: { store_id: storeId },
                success: function(response) {
                    if (response === "success") {                        
                        $("#row_" + storeId).remove();
                        alert("Delete Successfully.");
                    } else {
                        alert("Delete Successfully");
                    }
                },
                error: function(error) {
                    console.error(error);
                    alert("Failed to delete store.");
                }
            });
        }
    }
    </script> 
