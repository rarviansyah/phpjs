<?php
include 'db_connect.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $customer_name = $_POST['customer_name'];
    $address = $_POST['address'];
    $post_code = $_POST['post_code'];
    $phone_number = $_POST['phone_number'];

    // Menghasilkan nama unik untuk file gambar
    $profile_image = "gambar/" . uniqid() . "_" . $_FILES["profile_image"]["name"];

    // Pindahkan file gambar ke direktori uploads
    move_uploaded_file($_FILES["profile_image"]["tmp_name"], $profile_image);

    $sql = "INSERT INTO customers (customer_name, address, post_code, phone_number, profile_image) 
            VALUES ('$customer_name', '$address', '$post_code', '$phone_number', '$profile_image')";

    $query = "SELECT MAX(SUBSTRING(customer_code, 3)) as max_code FROM customers";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $max_code = $row['max_code'];
    $customer_code = "AC" . str_pad(($max_code ), 4, '0', STR_PAD_LEFT);

    if ($conn->query($sql) === TRUE) {
        header("Location: customer_table.php?message=Customer added successfully");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>customer</title>
</head>
<body>
  <a href="index.php">Dashboard</a>
  <a href="customer_table.php">Table</a>
  
  <h2>Add Customer</h2>
<form action="customer.php" method="post" enctype="multipart/form-data">
    <label for="customer_name">Customer Name:</label>
    <input type="text" name="customer_name" required>
    <br>
    <label for="address">Address:</label>
    <input type="text" name="address">
    <br>
    <label for="post_code">Post Code:</label>
    <input type="text" name="post_code">
    <br>
    <label for="phone_number">Phone Number:</label>
    <input type="text" name="phone_number">
    <br>
    <label for="profile_image">Profile Image:</label>
    <input type="file" name="profile_image" accept="image/*" required>
    <br>
    <input type="submit" value="Add Customer">
</form>
</body>
</html>