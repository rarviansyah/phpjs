<?php
include 'db_connect.php';

$customer_code = $_GET['customer_code'];
$ctr = query("select * from customers where customer_code='$customer_code'")[0];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Customer</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
  <a href="index.php">Dashboard</a><br>
  <a href="customer_table.php">Table Customer</a><br>

<form method="post" id="updateCustomerForm">
        <label for="customer_name">Customer Name:</label>
        <input type="text" name="customer_name" value="<?php echo $ctr['customer_name']; ?>"><br>
        <label for="address">Address:</label>
        <input type="text" name="address" value="<?php echo $ctr['address']; ?>"><br>
        <label for="post_code">Post Code:</label>
        <input type="text" name="post_code" value="<?php echo $ctr['post_code']; ?>"><br>
        <label for="phone_number">Phone Number:</label>
        <input type="text" name="phone_number" value="<?php echo $ctr['phone_number']; ?>"><br>
        <button type="submit" value="update" name="update" onclick="updateCustomer()">Update Customer</button>
    </form>

    <?php
      if(isset($_POST['update'])){
        $customer_name = $_POST['customer_name'];
        $address = $_POST['address'];
        $post_code = $_POST['post_code'];
        $phone_number = $_POST['phone_number'];

        mysqli_query($conn, "update customers set customer_name='$customer_name', address='$address', post_code='$post_code', phone_number='$phone_number' where customer_code='$customer_code'");
        echo "<script>alert('data successfully updated');window.location='customer_table.php';</script>";
      }
    ?>

    <script>
        function updateCustomer() {            
            $.ajax({
                type: "POST",
                url: "customer_table.php",
                data: $("#updateCustomerForm").serialize(),
                success: function(response) {
                    if (response === "success") {
                        alert("Update Successful.");
                    } else {
                        // alert("Failed to update store.");
                    }
                },
                error: function(error) {
                    console.log(error);
                    alert("Failed update data customer");
                }
            });
        }
    </script>
</body>
</html>
