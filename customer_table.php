<?php
include 'db_connect.php';

// Ambil data dari tabel customers
$sql = "SELECT * FROM customers";
$result = $conn->query($sql);

    if (isset($_POST['customer_code'])) {
        $customer_code = $_POST['customer_code'];      
        $delete_query = "DELETE FROM customers WHERE customer_code = '$customer_code'";
        if ($conn->query($delete_query) === TRUE) {
            echo "success";
        } else {
            echo "error";
        }
    } else {
        // echo "missing_customer_code";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Customer Table</title>
  <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
  <a href="customer.php">Add Customer</a>
  <a href="index.php">Dashboard</a>
  
  <h2>Customer Table</h2>
<table border="2" >
    <tr>
        <th>Customer Code</th>
        <th>Customer Name</th>
        <th>Address</th>
        <th>Post Code</th>
        <th>Phone Number</th>
        <th>Profile Image</th>
    </tr>

    <?php    
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<tr>
                    <td>{$row['customer_code']}</td>
                    <td>{$row['customer_name']}</td>
                    <td>{$row['address']}</td>
                    <td>{$row['post_code']}</td>
                    <td>{$row['phone_number']}</td>";

            // Periksa apakah kunci 'profile_image' ada dalam array $row
            if (isset($row['profile_image'])) {
                echo "<td><img src='{$row['profile_image']}' alt='Profile Image' width='50' align-items='center'></td>";
            } else {
                echo "<td>No Image</td>";
            }
            echo "<td><button onclick='deleteCustomer(\"{$row['customer_code']}\")'> Delete </button></td>";
            echo "<td><button> <a href='update_customer.php?customer_code=" . $row['customer_code'] . "'> Update </button></td>";

            echo "</tr>";
        }
    } else {
        echo "<tr><td colspan='6'>No customers found</td></tr>";
    }
    ?>
</table>    

<script>
    function deleteCustomer(customerCode) {
        if (confirm("Are you sure you want to delete this customer?")) {
            $.ajax({
                type: "POST",
                url: "customer_table.php", 
                data: { customer_code: customerCode },
                success: function(response) {
                    if (response === "success") {                        
                        $("#row_" + customerCode).remove();
                        alert("Delete Successfully");
                    } else {
                        alert("Delete Customer Successfully");
                        window.onload = setupRefresh;
                    }
                },
                error: function(xhr, status, error) {
                console.error(xhr.responseText);
                alert("Failed to delete customer. Server error.");
            }
            });
        }
    }
</script>

</body>
</html>
