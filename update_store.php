<?php
include 'db_connect.php';

$store_id = $_GET['store_id'];
$str = query("select * from stores where store_id='$store_id'")[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update Store</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
    <a href="save_store.php">Table Store</a>
    <a href="index.php">Dashboard</a>

    <h2>Update Store</h2>

    <form method="post" id="updateForm">        
    <label for="store_name">Store Name:</label>
    <input type="text" name="store_name" value="<?= $str['store_name']; ?>"><br>
    
    <label for="owner_name">Owner Name:</label>
    <input type="text" name="owner_name" value="<?= $str['owner_name']; ?>"><br>
    
    <label for="store_address">Store Address:</label>
    <input type="text" name="store_address" value="<?= $str['store_address']; ?>"><br>
    
    <button type="submit" name="update" value="update" onclick="updateStore()">Update Store</button>
</form>


    <?php
      if(isset($_POST['update'])){
        $store_name = $_POST['store_name'];
        $owner_name = $_POST['owner_name'];
        $store_address = $_POST['store_address'];

        mysqli_query($conn, "UPDATE stores SET store_name = '$store_name', owner_name = '$owner_name', store_address = '$store_address' WHERE store_id = $store_id");

        echo "<script>alert('data successfully updated');window.location='save_store.php';</script>";
      }

    ?>

    <script>
        function updateStore() {
            var formData = $("#updateForm").serialize();

            $.ajax({
                type: "POST",
                url: "save_store.php",
                data: formData,
                success: function(response) {
                    if (response === "success") {
                        alert("Update Successful.");
                    } else {
                        // alert("Failed to update store.");
                    }
                },
                error: function(error) {
                    console.error(error);
                    alert("Failed to update store.");
                }
            });
        }
    </script>
</body>
</html>