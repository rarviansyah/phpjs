<?php
include 'db_connect.php';

// Ambil data dari form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $store_id = $_POST['store_id'];
  $store_name = $_POST['store_name'];
  $owner_name = $_POST['owner_name'];
  $store_address = $_POST['store_address'];

  // Query untuk memeriksa apakah store_id sudah ada
  $check_duplicate_sql = "SELECT COUNT(*) FROM stores WHERE store_id = ?";
  $check_stmt = $conn->prepare($check_duplicate_sql);
  $check_stmt->bind_param("s", $store_id);
  $check_stmt->execute();
  $check_stmt->bind_result($count);
  $check_stmt->fetch();
  $check_stmt->close();

  // Jika store_id sudah ada, tampilkan pesan kesalahan
  if ($count > 0) {
      echo "Error: Duplicate entry for store_id";
  } else {
      // Query untuk menyimpan data ke database
      $insert_sql = "INSERT INTO stores (store_id, store_name, owner_name, store_address) VALUES (?, ?, ?, ?)";
      $insert_stmt = $conn->prepare($insert_sql);
      $insert_stmt->bind_param("ssss", $store_id, $store_name, $owner_name, $store_address);

      if ($insert_stmt->execute()) {
          echo "Data has been saved successfully";          
      } else {
          echo "Error: " . $insert_stmt->error;
      }

      $insert_stmt->close();
  }
}

    // if(isset($_POST['submit'])){
    //     $store_id = $_POST['store_id'];
    //     $store_name = $_POST['store_name'];
    //     $owner_name = $_POST['owner_name'];
    //     $store_address = $_POST['store_address'];

    //     if($store_id > 0) {
    //         echo "Error: Duplicate entry for store_id";
    //     } else {
    //         $sql = "INSERT INTO stores (store_id, store_name, owner_name, store_address) VALUES ($store_id, $store_name, $owner_name, $store_address)";
    //     }

    //     if ($conn->query($sql) === TRUE) {
    //         header("Location: save_store.php?message=Store added successfully");
    //         exit();
    //     } else {
    //         echo "Error: " . $sql . "<br>" . $conn->error;
    //     }
    // }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Store Form</title>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
<body>
<br>
<a href="index.php">Dashboard</a>
<a href="save_store.php">Tabel Store</a>

    <form action="store.php" id="storeForm" method="post">
        <label for="store_id">Store ID:</label>
        <input type="text" name="store_id" id="store_id" required><br>

        <label for="store_name">Store Name:</label>
        <input type="text" name="store_name" id="store_name" required><br>

        <label for="owner_name">Owner Name:</label>
        <input type="text" name="owner_name" id="owner_name" required><br>

        <label for="store_address">Store Address:</label>
        <textarea name="store_address" id="store_address" required></textarea><br>

        <input type="submit" name="submit" value="add store">
    </form>
</body>
</html>
